/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog5
    CS530, Spring 2016
    Optional enhancements: Cascading forward references (unlimited levels).
*/

#ifndef FILE_PARSE_EXCEPTION_H
#define FILE_PARSE_EXCEPTION_H
#include <iostream>

using namespace std;

class file_parse_exception {

public:
    file_parse_exception(string s) {
        message = s;        
        }
        
    file_parse_exception() {
        message = "An error has occurred";
        }
        
    string getMessage() {
        return message;
    }
    
private:
    string message;
};    

#endif
