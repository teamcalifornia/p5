/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog5
    CS530, Spring 2016
    Optional enhancements: Cascading forward references (unlimited levels).
*/

#ifndef SYMTAB_H
#define SYMTAB_H

#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include "file_parser.h"

using namespace std;

class symtab {

    public:
	
	// ctor - takes in file_parser output
	// and stores the data into the private variable
        symtab(); 

	// contains try and catch block
	// calls generate symtab
	void read_data(file_parser);

	// Add label to map 
	// return false if label already in map
	void add_symbol_to_map(string, unsigned int, unsigned int);

	// Get address of given label string
	// if no label found return -2
	int get_address_of_symbol(string);	

	// Find address in map and when found
	// update map with address	
	bool update_symbol_with_address(string, int);

	// Generate symbols with given data
	// in the form of the file_parser
	// throw exception if any errors
	void generate_symtab_with_data(file_parser);
	
	// Print contents of symtab
	void print_file();
	
	// Check to see if symbol is in symtab
	bool symbol_exists(string);

    private:

	map <string, int> symbol_map;
	map <string, int>::iterator iter;
	
	vector <string> assembler_directives;

	void init_assembler_directives();
	
	string get_uppercase(string);
};

#endif
