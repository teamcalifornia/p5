/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog5
    CS530, Spring 2016
    Optional enhancements: Cascading forward references (unlimited levels).
*/

#include "symtab.h"
#include "symtab_exception.h"
#include "file_parser.h"

using namespace std;

symtab::symtab() {

}

void symtab::add_symbol_to_map(string symbol, unsigned int address, unsigned int line) {

	if(symbol.empty()) {
		
		std::ostringstream e;
		e << "Error: Blank symbol not allowed on line " << line + 1;
		throw(symtab_exception(e.str()));

	}
	if(symbol_exists(symbol) == false) {

		symbol_map.insert( pair <string, int> (get_uppercase(symbol), address) );

	} else {

		std::ostringstream e;
		e << "Error: Duplicate symbol found on line " << line + 1;
		throw(symtab_exception(e.str()));

	}

}

int symtab::get_address_of_symbol(string symbol) {

	iter = symbol_map.find(get_uppercase(symbol));

	if(iter != symbol_map.end()) {

		return iter->second;

	}

	return -1;
}

bool symtab::update_symbol_with_address(string symbol, int address) {

	iter = symbol_map.find(get_uppercase(symbol));
	if( iter != symbol_map.end() ) {
		iter->second = address;
		return true;
	}

	return false;

}

string symtab::get_uppercase(string text) {

	transform(text.begin(), text.end(), text.begin(), ::toupper);
	return text;

}

bool symtab::symbol_exists(string label) {

	if(label.empty()) return false;
	if(symbol_map.find(get_uppercase(label)) == symbol_map.end()) return false;
	return true;

}

void symtab::print_file() {
	for(iter = symbol_map.begin(); iter != symbol_map.end(); iter++) {

		cout << iter->first << " " << iter->second << endl;

	}
}
